package io.keepcoding.spark.exercise.batch

import java.sql.Timestamp
import java.time.OffsetDateTime
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

import org.apache.spark.sql.{DataFrame, SparkSession}

case class AntennaMessage(year: Int, month: Int, day: Int, hour: Int, timestamp: Timestamp, id: String, metric: String, value: Long)

trait BatchJob {

  val spark: SparkSession

  def readFromStorage(storagePath: String, filterDate: OffsetDateTime): DataFrame

  def readUserMetadata(jdbcURI: String, jdbcTable: String, user: String, password: String): DataFrame

  def enrichAntennaWithMetadata(antennaDF: DataFrame, metadataDF: DataFrame): DataFrame

  def computeTotalBytesByAntenna(dataFrame: DataFrame): DataFrame

  def computeTotalBytesByUser(dataFrame: DataFrame): DataFrame

  def computeTotalBytesByApp(dataFrame: DataFrame): DataFrame

  def computeExceededQuotaByUserEmail(usageDF: DataFrame): DataFrame

  def writeToJdbc(dataFrame: DataFrame, jdbcURI: String, jdbcTable: String, user: String, password: String): Unit


  def run(args: Array[String]): Unit = {
    val Array(filterDate, storagePath, jdbcUri, jdbcMetadataTable, aggJdbcTable, aggJdbcLimitQuotaTable,  jdbcUser, jdbcPassword) = args
    println(s"Running with: ${args.toSeq}")

    val antennaDF = readFromStorage(storagePath, OffsetDateTime.parse(filterDate))
    val metadataDF = readUserMetadata(jdbcUri, jdbcMetadataTable, jdbcUser, jdbcPassword)
    val antennaMetadataDF = enrichAntennaWithMetadata(antennaDF, metadataDF).cache()
    val aggByAntennaDF = computeTotalBytesByAntenna(antennaDF)
    val aggByUserDF = computeTotalBytesByUser(antennaDF)
    val aggByAppDF = computeTotalBytesByApp(antennaDF)
    val limitQuota = computeExceededQuotaByUserEmail(antennaMetadataDF)

    writeToJdbc(aggByAntennaDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)
    writeToJdbc(aggByUserDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)
    writeToJdbc(aggByAppDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)
    writeToJdbc(limitQuota, jdbcUri, aggJdbcLimitQuotaTable, jdbcUser, jdbcPassword)


    spark.close()
  }

}
