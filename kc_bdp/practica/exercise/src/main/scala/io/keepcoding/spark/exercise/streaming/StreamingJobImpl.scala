package io.keepcoding.spark.exercise.streaming
import scala.concurrent.{Await, Future}
import org.apache.spark.sql.catalyst.ScalaReflection
import org.apache.spark.sql.types.{StringType, StructType}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.lit

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
object StreamingJobImpl extends StreamingJob {
  override val spark: SparkSession =
    SparkSession
      .builder()
      .master("local[*]")
      .appName("Streaming Job")
      .getOrCreate()
  import spark.implicits._
  override def readFromKafka(kafkaServer: String, topic: String): DataFrame =
    spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", kafkaServer)
      .option("subscribe", topic)
      .load()
  override def parserJsonData(dataFrame: DataFrame): DataFrame = {
    val schema = ScalaReflection.schemaFor[AntennaMessage].dataType.asInstanceOf[StructType]
    dataFrame
      .select(from_json($"value".cast(StringType), schema).as("json"))
      .select("json.*")
  }

  override def computeTotalBytesByAntenna(dataFrame: DataFrame): DataFrame = {
    dataFrame
      .select($"timestamp", $"antenna_id", $"bytes")
      .withWatermark("timestamp", "1 minute")
      .groupBy($"antenna_id".as("id"), window($"timestamp", "5 minutes" ).as("w"))
      .agg(sum($"bytes").as("value")
      )
      .select($"w.start".as("timestamp"), $"id", $"value")
      .withColumn("type", lit("antenna_bytes_total"))
  }

  override def computeTotalBytesByUser(dataFrame: DataFrame): DataFrame = {
    dataFrame
      .select($"timestamp", $"id", $"bytes")
      .withWatermark("timestamp", "1 minute")
      .groupBy($"id", window($"timestamp", "5 minutes" ).as("w"))
      .agg(sum($"bytes").as("value")
      )
      .select($"w.start".as("timestamp"), $"id", $"value")
      .withColumn("type", lit("user_bytes_total"))
  }
  override def computeTotalBytesByApp(dataFrame: DataFrame): DataFrame = {
    dataFrame
      .select($"timestamp", $"app", $"bytes")
      .withWatermark("timestamp", "1 minute")
      .groupBy($"app".as("id"), window($"timestamp", "5 minutes" ).as("w"))
      .agg(sum($"bytes").as("value")
      )
      .select($"w.start".as("timestamp"), $"id", $"value")
      .withColumn("type", lit("app_bytes_total"))
  }


  override def writeToJdbc(dataFrame: DataFrame, jdbcURI: String, jdbcTable: String, user: String, password: String): Future[Unit] = Future {

    dataFrame
      .writeStream
      .foreachBatch{ (df:DataFrame, id: Long) =>
      df
        .write
        .mode(SaveMode.Append)
        .option("driver", "org.postgresql.Driver")
        .format("jdbc")
        .option("url", jdbcURI)
        .option("dbtable", jdbcTable)
        .option("user", user)
        .option("password", password)
        .save()
        }.start()
        .awaitTermination()


  }
  override def writeToStorage(dataFrame: DataFrame, storageRootPath: String): Future[Unit] = Future {
    dataFrame
      .withColumn("year", year($"timestamp"))
      .withColumn("month", month($"timestamp"))
      .withColumn("day", dayofmonth($"timestamp"))
      .withColumn("hour", hour($"timestamp"))
      .writeStream
      .partitionBy("year", "month", "day", "hour")
      .format("parquet")
      .option("path", s"$storageRootPath/data")
      .option("checkpointLocation", s"$storageRootPath/checkpoint")
      .start()
      .awaitTermination()
  }

  def main(args: Array[String]): Unit = run(Array("35.228.118.132:9092","devices", "jdbc:postgresql://35.228.122.99:5432/postgres", "bytes", "postgres", "keepcoding","/tmp/practica/"))

/*
  def main(args: Array[String]): Unit = {

    //val metadataDF = readAntennaMetadata("jdbc:postgresql://35.228.122.99:5432/postgres", "user_metadata", "postgres", "keepcoding")

    val antennaStreamDF = parserJsonData(readFromKafka("35.228.118.132:9092", "devices"))

    val aggAntenna = writeToJdbc(computeTotalBytesByAntenna(antennaStreamDF),
      "jdbc:postgresql://35.228.122.99:5432/postgres", "bytes", "postgres", "keepcoding"
    )

    val aggUser = writeToJdbc(computeTotalBytesByUser(antennaStreamDF),
      "jdbc:postgresql://35.228.122.99:5432/postgres", "bytes", "postgres", "keepcoding"
    )

    val aggApp = writeToJdbc(computeTotalBytesByApp(antennaStreamDF),
      "jdbc:postgresql://35.228.122.99:5432/postgres", "bytes", "postgres", "keepcoding"
    )

    val writeToStorageFut = writeToStorage(antennaStreamDF, "/tmp/practica/")

    val aggFut = Future.sequence(Seq(aggAntenna, aggUser, aggApp,writeToStorageFut))

    Await.result(aggFut, Duration.Inf)

  }
*/
}


