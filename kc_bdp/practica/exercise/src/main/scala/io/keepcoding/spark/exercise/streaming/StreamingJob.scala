package io.keepcoding.spark.exercise.streaming

import java.sql.Timestamp
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

import org.apache.spark.sql.{DataFrame, SparkSession}

case class AntennaMessage(timestamp: Timestamp, id: String, antenna_id: String, bytes: Long, app: String)

trait StreamingJob {

  val spark: SparkSession

  def readFromKafka(kafkaServer: String, topic: String): DataFrame

  def parserJsonData(dataFrame: DataFrame): DataFrame

  //def readAntennaMetadata(jdbcURI: String, jdbcTable: String, user: String, password: String): DataFrame

  //def enrichAntennaWithUserMetadata(antennaDF: DataFrame, metadataDF: DataFrame): DataFrame

  def computeTotalBytesByAntenna(dataFrame: DataFrame): DataFrame

  def computeTotalBytesByUser(dataFrame: DataFrame): DataFrame

  def computeTotalBytesByApp(dataFrame: DataFrame): DataFrame

  def writeToJdbc(dataFrame: DataFrame, jdbcURI: String, jdbcTable: String, user: String, password: String): Future[Unit]

  def writeToStorage(dataFrame: DataFrame, storageRootPath: String): Future[Unit]

  def run(args: Array[String]): Unit = {
    val Array(kafkaServer, topic, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword, storagePath) = args
    println(s"Running with: ${args.toSeq}")

    val kafkaDF = readFromKafka(kafkaServer, topic)
    val antennaDF = parserJsonData(kafkaDF)
    val storageFuture = writeToStorage(antennaDF, storagePath)
    val aggByAntennaDF = computeTotalBytesByAntenna(antennaDF)
    val aggByUserDF = computeTotalBytesByUser(antennaDF)
    val aggByAppDF = computeTotalBytesByApp(antennaDF)
    val aggAntennaFuture = writeToJdbc(aggByAntennaDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)
    val aggUserFuture = writeToJdbc(aggByUserDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)
    val aggAppFuture = writeToJdbc(aggByAppDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)

    val aggFuture = Future.sequence(Seq(aggAntennaFuture, aggUserFuture, aggAppFuture,storageFuture))
    Await.result(aggFuture, Duration.Inf)

    spark.close()
  }

}



