package io.keepcoding.spark.exercise.batch
import java.time.OffsetDateTime
import io.keepcoding.spark.exercise.streaming.StreamingJobImpl.{spark}
import org.apache.spark.sql.functions.{approx_count_distinct, avg, count, lit, max, min, sum, when, window}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object BatchJobImpl extends BatchJob {

  override val spark: SparkSession =
    SparkSession
      .builder()
      .master("local[*]")
      .appName("Batch Job")
      .getOrCreate()
  import spark.implicits._
  override def readFromStorage(storagePath: String, filterDate: OffsetDateTime): DataFrame = {
    spark
      .read
      .format("parquet")
      .load(storagePath)
      .where(
        $"year" === filterDate.getYear &&
          $"month" === filterDate.getMonthValue &&
          $"day" === filterDate.getDayOfMonth &&
          $"hour" === filterDate.getHour
      )
  }


  override def readUserMetadata(jdbcURI: String, jdbcTable: String, user: String, password: String): DataFrame = {
    spark
      .read
      .format("jdbc")
      .option("url", jdbcURI)
      .option("dbtable", jdbcTable)
      .option("user", user)
      .option("password", password)
      .load()
  }

  override def enrichAntennaWithMetadata(antennaDF: DataFrame, userMetadataDF: DataFrame): DataFrame = {
    antennaDF
      .join(userMetadataDF,
        antennaDF("id") === userMetadataDF("id"))
      .drop(userMetadataDF("id"))

  }

  override def computeTotalBytesByAntenna(dataFrame: DataFrame): DataFrame = {
    dataFrame
      .select($"timestamp", $"antenna_id", $"bytes")
      .groupBy($"antenna_id".as("id"), window($"timestamp", "1 hour" ).as("w"))
      .agg(sum($"bytes").as("value")
      )
      .select($"w.start".as("timestamp"), $"id", $"value")
      .withColumn("type", lit("antenna_bytes_total"))
  }
  override def computeTotalBytesByUser(dataFrame: DataFrame): DataFrame = {
    dataFrame
      .select($"timestamp", $"id", $"bytes")
      .groupBy($"id", window($"timestamp", "1 hour" ).as("w"))
      .agg(sum($"bytes").as("value")
      )
      .select($"w.start".as("timestamp"), $"id", $"value")
      .withColumn("type", lit("user_bytes_total"))
  }
  override def computeTotalBytesByApp(dataFrame: DataFrame): DataFrame = {
    dataFrame
      .select($"timestamp", $"app", $"bytes")
      .groupBy($"app".as("id"), window($"timestamp", "1 hour" ).as("w"))
      .agg(sum($"bytes").as("value")
      )
      .select($"w.start".as("timestamp"), $"id", $"value")
      .withColumn("type", lit("app_bytes_total"))
  }
  override def computeExceededQuotaByUserEmail(usageDF: DataFrame): DataFrame = {
    usageDF
      .select($"timestamp", $"email", $"bytes", $"quota")
      .groupBy($"email", $"quota", window($"timestamp", "1 hour" ).as("w"))
      .agg(sum($"bytes").as("usage"))
      .select($"email",  $"usage", $"quota", $"w.start".as("timestamp"))
      .drop("id")
      .drop("name")
      .filter($"usage" > $"quota")

  }

  override def writeToJdbc(dataFrame: DataFrame, jdbcURI: String, jdbcTable: String, user: String, password: String): Unit = {
    dataFrame
      .write
      .mode(SaveMode.Append)
      .option("driver", "org.postgresql.Driver")
      .format("jdbc")
      .option("url", jdbcURI)
      .option("dbtable", jdbcTable)
      .option("user", user)
      .option("password", password)
      .save()
  }


  def main(args: Array[String]): Unit = run(Array("2021-02-20T12:00:00Z","/tmp/practica/data", "jdbc:postgresql://35.228.122.99:5432/postgres", "user_metadata", "bytes_hourly", "user_quota_limit",  "postgres", "keepcoding")
  )


/*
  def main(args: Array[String]): Unit = {
    val argsTime = "2021-02-20T12:00:00Z"

    val storageDF = readFromStorage("/tmp/practica/data", OffsetDateTime.parse(argsTime))

    val userMetadataDF = readUserMetadata("jdbc:postgresql://35.228.122.99:5432/postgres", "user_metadata", "postgres", "keepcoding")
    val enrichDF = enrichAntennaWithMetadata(storageDF, userMetadataDF)
    val aggAntennaDF = computeTotalBytesByAntenna(storageDF)
    val aggUserDF = computeTotalBytesByUser(storageDF)
    val aggAppDF = computeTotalBytesByApp(storageDF)

    val limitQuotaDF = computeExceededQuotaByUserEmail(enrichDF)

    writeToJdbc(aggAntennaDF, "jdbc:postgresql://35.228.122.99:5432/postgres", "bytes_hourly", "postgres", "keepcoding")
    writeToJdbc(aggUserDF, "jdbc:postgresql://35.228.122.99:5432/postgres", "bytes_hourly", "postgres", "keepcoding")
    writeToJdbc(aggAppDF, "jdbc:postgresql://35.228.122.99:5432/postgres", "bytes_hourly", "postgres", "keepcoding")
    writeToJdbc(limitQuotaDF, "jdbc:postgresql://35.228.122.99:5432/postgres", "user_quota_limit", "postgres", "keepcoding")


  }
*/

}
