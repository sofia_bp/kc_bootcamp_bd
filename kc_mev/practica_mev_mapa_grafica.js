// Importamos los datos y cargamos la visualización

d3.json("Practica/practica_airbnb.json")
.then((featureCollection) => {
    drawMap(featureCollection);

});

// La función drawMap contiene la visualización del mapa y la gráfica XY
function drawMap(featureCollection) {

featureCollection.features.forEach((d, i) => {
        d.idx = i //Añadimos el index como propiedad de cada objeto (lo necesitamos para el .on listener)
        //console.log(d.idx)
})

//1. Datos GeoJSON

 var preciomax = d3.max(featureCollection.features, (d,i) => featureCollection.features[i].properties.avgprice);
 console.log(preciomax)
 var preciomin = d3.min(featureCollection.features, (d,i) => featureCollection.features[i].properties.avgprice);
 console.log(preciomin)


 // Variables generales para el lienzo de mapa svg1

var widthmap = 400;
var heightmap = 700;
var marginbottom = 50
var margintop = 50
var marginleft = 50
var marginright = 50
var svg1 = d3.select("#mapid")
    .append("svg")
    .attr("width", widthmap + marginleft + marginright)
    .attr("height", heightmap + marginbottom + margintop)
    .append("g")
    .attr("transform", "translate(" + marginleft + ",0)");

//2.Proyeccion de coordenadas [long,lat] en valores X,Y
var center = d3.geoCentroid(featureCollection)
var projection = d3.geoMercator()
    .fitSize([widthmap, heightmap], featureCollection) 

//3.Crear paths a partir de coordenadas proyectadas.
var pathGenerator = d3.geoPath().projection(projection);

/* Aplicamos una escala d3.scaleQuantize() con input continuo (array de precios) y output discreto 
(categorías de color)*/
var quantizeScale = d3.scaleQuantize()
    .domain([0, preciomax])
    .range(d3.schemeSet2);


// Creamos tantos paths como áreas tengamos --> método enter
var pathMap = svg1.append("g")
    .selectAll("path")
    .data(featureCollection.features) 
    .enter()
    .append("path")
    .attr("d", function(d) { return pathGenerator(d) }) // atributo típico de todo path--> d
    .attr("fill", function(d, i) {
        d.precio_medio = featureCollection.features[i].properties.avgprice
        if (d.precio_medio == null) {
            return "white"
         } else {
            return quantizeScale(d.precio_medio)}
    })
    .attr("stroke", "black")
    .attr("stroke-width", 0.5)
    .attr("opacity", 1);

var mapTitle = svg1.append("text")
    .attr("x", 0)
    .attr("y", marginbottom)
    .style("font-size", "18px") 
    .text("Precio medio de alquiler por barrio en Airbnb - Madrid")

var widthrect = 50;
var heightrect = 20;
var numRanges = 8;

// Array de rangos de precios para el texto de la leyenda:
let arrayPrices = []
for (var i =0; i< numRanges ;i++) {
    arrayPrices[i] = preciomax/numRanges*i + " - " + preciomax/numRanges*(i+1);
 }

var legend = svg1.append("g")
    .selectAll("rect")
    .data(d3.schemeSet2)
    .enter()
    .append("rect")
    .attr("x", function(d, i) {
        return (50 * i)
    })
    .attr("y", 0)
    .attr("width", widthrect)
    .attr("height", heightrect)
    .attr("fill", (d) => d)
    .attr("transform", "translate(0," + (heightmap - marginbottom) + ")");


var legend_text = svg1.append("g")
    .selectAll("text")
    .data(arrayPrices)
    .enter()
    .append("text")
    .attr("x", function(d, i) {
        return (50 * i)
    })
    .attr("y", heightrect + heightmap - marginbottom + 10)
    .attr("font-size", "11px")
    .text((d) => d)
    .attr("transform", "translate(5, " + 5 + ")");

var legent_title = svg1.append("text")
    .attr("x", 0)
    .attr("y", heightmap - marginbottom - heightrect + 10)
    .style("font-size", "18px") 
    .text("Rangos de precio medio en Eur")

// Barrio Ibiza para gráfico inicial
var b = 16
//d3.json("Practica/practica_airbnb.json")
//.then((featureCollection) => {
//    drawGraph(featureCollection,b);
//});

/*  ##### GRÁFICA ##### */

var heightgraph = 400;
var widthgraph = 500;
var marginbottom = 100;
var margintop = 100;
var marginleft = 50;

var svg2 = d3.select('#graficaXY')
    .append('svg')
    .attr('width', widthgraph)
    .attr('height', heightgraph + marginbottom + margintop)
    .append("g")
    .attr("transform", "translate(0," + margintop + ")");
    

    var yaxis_title = svg2.append("text")
    .attr("x", marginleft)
    .attr("y", -5)
    .style("font-size", "14px") 
    .text("Número de propiedades");

    var xaxis_title = svg2.append("text")
    .attr("x", widthgraph - 150 )
    .attr("y", heightgraph + 30)
    .style("font-size", "14px") 
    .text("Número de habitaciones");

// Gráfica inicial con barrio seleccionado

//1. Selección de barrio
//console.log(featureCollection)



console.log(featureCollection.features[b].properties.avgbedrooms)
//Creacion de escalas
var xscale = d3.scaleBand()
    .domain(featureCollection.features[b].properties.avgbedrooms.map(function(d) {
        //console.log(d.bedrooms)
        return d.bedrooms;
    }))
    .range([0, widthgraph - marginleft - marginright])
    .padding(0.1);

var yscale = d3.scaleLinear()
    .domain([0, d3.max(featureCollection.features[b].properties.avgbedrooms, function(d) {
        //console.log(d.total)
        return d.total;
    })])
    .range([heightgraph, 0]);

//Creación de eje X
var xaxis = d3.axisBottom(xscale);
//Añadimos el eje X
svg2.append("g")
    .attr("transform", "translate(" + marginleft + "," + heightgraph + ")")
    .attr("class","Xaxis")
    .call(xaxis);

//Creación de eje Y
var yaxis = d3.axisLeft(yscale).ticks(5)
//Añadimos el eje Y
svg2.append("g")
    .attr("transform", "translate(" + marginleft + ", 0)")
    .attr("class","Yaxis")
    .call(yaxis);


//Ejecutamos la gráfica con el barrio inicial
drawGraph(featureCollection,b)



pathMap.on("click", function(event, d){
    d3.selectAll(".rect-target")
        .remove()
    //d3.selectAll(".Xaxis")
      //  .remove()
    //d3.selectAll(".Yaxis")
    //    .remove()
    d3.selectAll(".text-target")
        .remove()
    d3.selectAll(".bar-text")
        .remove()
        
    handleClick(event, d)

 })

function handleClick(event, d) {   
        d3.selectAll(this)
        xscale.domain(featureCollection.features[d.idx].properties.avgbedrooms.map(function(d) {
            return d.bedrooms;
        }))
        d3.select(".Xaxis")
            .transition()
            .duration(100)
            .call(xaxis)
        yscale.domain([0, d3.max(featureCollection.features[d.idx].properties.avgbedrooms, function(d, i) {
            return d.total;
        })]) 
        d3.select(".Yaxis")
            .transition()
            .duration(500)
            .call(yaxis)
        drawGraph(featureCollection, d.idx);
        }

// Función para la actualización de la gráfica
function drawGraph(featureCollection, b) {

var quarterprop = featureCollection.features[b].properties
console.log(quarterprop.avgbedrooms)


var rect = svg2.append("g")
    .attr("class", "rect-target")
    .selectAll("rect")
    .data(quarterprop.avgbedrooms)
    .enter()
    .append("rect")
    .attr("x", function(d) { return xscale(d.bedrooms)})
    .attr("y", 0) //function(d) { return yscale(d.total)})
    .attr("width", xscale.bandwidth())
    .attr("height", function(d) {
        return heightgraph - yscale(0)}) //yscale(d.total)})
    .attr("transform", "translate(" + marginleft + ", 0)")
    .attr("fill", "aquamarine");

    rect.transition("t0") 
    .ease(d3.easeLinear)
    .duration(500)
    .delay(function(d, i) {
        //console.log(i);
        return (i * 300)
    })
    .attr('y', function(d) { return yscale(d.total)})
    .attr("height", function(d) {
        return heightgraph - yscale(d.total)}); //Altura real de cada rectangulo.
    
    

  //Texto sobre las barras
var bar_text = svg2.append("g")
    .attr("class", "bar-text")
    //.style("visibility", "hidden")
    .selectAll("text")
    .data(quarterprop.avgbedrooms)
    .enter()
    .append('text')
    .attr("id", (d, i) => "text" + i) //Asociamos un id a cada texto.
    .attr('x', d => xscale(d.bedrooms) + xscale.bandwidth())
    .attr('y', d => yscale(d.total - d.total/2))
    .text(d => d.total)
    .attr("font-size", 14)
    .attr("fill", "MidnightBlue")
    .attr("transform", "translate(10,0)")
    .attr("opacity", 0);

    bar_text.transition()
    .duration(2000)
    .attr("opacity", 1)

    var graphTitle = svg2
    .append("text")
    .attr("class", "text-target")
    .attr("x", 50)
    .attr("y", -50)
    .style("font-size", "16px") 
    .text(`Barrio: ${quarterprop.name}, Precio medio: ${quarterprop.avgprice} Eur`)

    }

 
}
